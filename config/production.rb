Rails.application.configure do
	config.serve_static_assets = true
    config.assets.compile = true
    config.assets.digest = true
	config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect'

end