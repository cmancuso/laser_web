// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//


//= require turbolinks
//= require bootstrap-sprockets
//= require better-simple-slideshow
//= require better-simple-slideshow.min
//= require jquery
//= require_tree .

// var opts = {
//             //auto-advancing slides? accepts boolean (true/false) or object
//             auto : { 
//                 // speed to advance slides at. accepts number of milliseconds
//                 speed : 2500, 
//                 // pause advancing on mouseover? accepts boolean
//                 pauseOnHover : true 
//             },
//             // show fullscreen toggle? accepts boolean
//             fullScreen : true, 
//             // support swiping on touch devices? accepts boolean, requires hammer.js
//             swipe : true 
//         };

// makeBSS('.bss-slides', opts);